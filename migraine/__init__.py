import sqlite3
import typing

from .single_assign_dict import *

FwdMigrationType = typing.Union[str, typing.Callable[[typing.Any], typing.Any]]
BackMigrationType = typing.Union[str, typing.Callable[[typing.Any], typing.Any], None]


class step(object):
    def __init__(
            self,
            fwd: FwdMigrationType,
            back: BackMigrationType = None) -> None:
        self.fwd = fwd
        self.rev = back

    @property
    def fwd(self) -> FwdMigrationType:
        return self.__fwd

    @fwd.setter
    def fwd(self, fwd: FwdMigrationType) -> None:
        if isinstance(fwd, str) or isinstance(fwd, typing.Callable):  # type: ignore
            self.__fwd = fwd
        else:
            raise ValueError('Forward step must be str or callable')

    @property
    def back(self) -> BackMigrationType:
        return self.__back

    @back.setter
    def back(self, back: BackMigrationType = None) -> None:
        if back is None or isinstance(back, str) or isinstance(back, callable):  # type: ignore
            self.__back = back
        else:
            raise ValueError('Backward step must be str, callable or None')


class Migrations(metaclass=RedefBlocker):
    def __init__(
            self,
            db_connection: sqlite3.Connection,
            table_name: str = "__migraine__"
    ) -> None:
        self.__conn = db_connection
        self.__table_name = table_name
        self.__create_log()

    def __create_log(self) -> None:
        self.__conn.execute("""\
            CREATE TABLE IF NOT EXISTS {} (
                step_name TEXT PRIMARY KEY
                , applied_timestamp DATETIME NOT NULL
            );""".format(self.__table_name))
        self.__conn.commit()

    def list_applied_migrations(self) -> typing.List[str]:
        c = self.__conn.cursor()
        c.execute("""\
            SELECT step_name
            FROM {}
            ORDER BY step_name ASC
            ;""".format(self.__table_name))
        return [r[0] for r in c.fetchall()]

    def list_available_migrations(self) -> typing.List[str]:
        return [
            e for e in dir(self)
            if e.startswith('mig_') and callable(self.__getattribute__(e))
        ]

    def list_pending_migrations(self) -> typing.List[str]:
        return sorted(
            set(self.list_available_migrations())
            - set(self.list_applied_migrations())
        )

    def apply_pending_migrations(self) -> None:
        for m in self.list_pending_migrations():
            self.apply_migration(m)
        return None

    def apply_migration(self, migration: FwdMigrationType) -> None:
        # # TODO: extract to "check migrations are sane"
        # for s in self.__getattribute__(migration)():
        #     if not isinstance(s, step):
        #         raise TypeError(
        #             '{} returned {}, expected step() instance'
        #                 .format(migration.__name__, type(s))
        #         )

        for s in self.__getattribute__(migration)():  # type: ignore
            fwd = s.fwd
            if isinstance(fwd, str):
                c = self.__conn.cursor()
                c.executescript(fwd)
            else:
                fwd(self.__conn)
        self.__conn.execute("""\
            INSERT INTO {} (step_name, applied_timestamp)
            VALUES (:step_name, CURRENT_TIMESTAMP);""".format(self.__table_name),
            {'step_name': migration})
        self.__conn.commit()
        return None


class JNXLogMigrations(Migrations):
    @staticmethod
    def mig_0001_create_locations_table() -> typing.Generator[step, None, None]:
        yield step(
            fwd="""\
               CREATE TABLE IF NOT EXISTS locations (
                    programme VARCHAR(8) NOT NULL
                    , association VARCHAR(8) NOT NULL
                    , reference VARCHAR(16) NOT NULL
                    , name VARCHAR(100) NOT NULL
                    , latitude DECIMAL(8, 5)
                    , longitude DECIMAL(8, 5)
                    , location_inaccurate BOOL
                    , valid_from DATE
                    , valid_to DATE
                    , PRIMARY KEY (programme, reference)
                );""",
            back="DROP TABLE locations;"
        )


if __name__ == '__main__':
    import sqlite3

    jnxlogmigrations = JNXLogMigrations(
        db_connection=sqlite3.connect(':memory:'),
    )

    print(jnxlogmigrations.list_pending_migrations())
    jnxlogmigrations.apply_pending_migrations()
    print(jnxlogmigrations.list_pending_migrations())
    print(jnxlogmigrations.list_applied_migrations())

